import express from 'express'
const app = express();
const bodyParser = require("body-parser");
require('dotenv').config()
const port = 5000
const route = require('./app/router/router')
import db from './app/models'
db.sequelize.sync();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

db.sequelize.sync();
app.use('/api/emp', route);
app.get('/', function (req, res) {
  res.sendFile("D:/TSWithSequlize/app/index.html" )
});

app.listen(port, () => {
  console.log(`app listening on port ${port}`)
})
