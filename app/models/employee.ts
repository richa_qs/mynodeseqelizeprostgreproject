'use strict';
import {
  Model
} from 'sequelize';
const {Projects,Employees, ProjectAssignment} = require( './../config/association')
interface EmpAttributes  {
  id: number,
  firstname: string,
  lastname:string,
  phone:string
  
};
module.exports = (sequelize:any, DataTypes:any) => {
  
  class Employee extends Model <EmpAttributes> implements EmpAttributes {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
     id!: number;
     firstname!: string;
     lastname!:string;
     phone!:string;
    static associate(models:any) {
    console.log(models)
      Employees.belongsToMany(Projects,{
        through:'ProjectAssignments'
      })
      // define association here
    }
  }
  Employee.init({
    id:{
      type:DataTypes.INTEGER,
      allowNull:false,
      primaryKey:true,
      autoIncrement:true
    },
   
    firstname: {
      type:DataTypes.STRING,
      allowNull:false
    },
    lastname:{
      type:DataTypes.STRING,
      allowNull:false
    },
    phone :{
      type:DataTypes.STRING,
      allowNull:false,
      unique:true
    }
  }, {
    sequelize,
    modelName: 'Employee',
  });
  return Employee;
};