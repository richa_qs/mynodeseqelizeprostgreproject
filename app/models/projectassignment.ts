'use strict';
import {
  Model
} from 'sequelize';
interface ProjectAssignmentAttributes {
 ProjectId:number,
 EmployeeId:number
  
};
module.exports = (sequelize:any, DataTypes:any) => {
  class ProjectAssignment extends Model<ProjectAssignmentAttributes> 
  implements ProjectAssignmentAttributes {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
     ProjectId!:number;
     EmployeeId!:number;
    static associate(models:any) {
      // define association here
    }
  }
  ProjectAssignment.init({
    ProjectId:{
      type:DataTypes.INTEGER,
      allowNull:false,
      primaryKey:true,
      references:{
        model:"Projects",
        key:'id'
      }
    },
    EmployeeId:{
      type:DataTypes.INTEGER,
      allowNull:false,
      primaryKey:true,
      references:{
        model:"Employees",
        key:'id'
      }
    }
  }, {
    sequelize,
    modelName: 'ProjectAssignment',
  });
  return ProjectAssignment;
};