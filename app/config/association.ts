import db from '../models/index'
const Sequelize=db.Sequelize;
const sequelize= db.sequelize;
const ProjectModel = require('../models/project');
const EmployeeModel = require('../models/employee');
const ProjectAssignmentModel = require('../models/projectassignment');
const Projects = ProjectModel(sequelize, Sequelize);
const Employees = EmployeeModel(sequelize, Sequelize);
const ProjectAssignment= ProjectAssignmentModel(sequelize, Sequelize);
Employees.belongsToMany(Projects,{
  through:'ProjectAssignments'
})
Projects.belongsToMany(Employees,{
  through:'ProjectAssignments'
})
module.exports = {
    Projects,
    Employees,
    ProjectAssignment
  };
