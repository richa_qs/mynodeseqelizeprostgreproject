// const express = require('express');
import express from 'express'
const router = express.Router();
const employeeController = require("../controllers/employeeController");
const projectController = require("../controllers/projectController");
const projectAssignmentController = require("../controllers/projectAssignmentController");
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })
router.post("/createEmployee",async (req: any,res: any) => {
    await employeeController.createEmployee(req,res);
})
router.get("/getAll",async (req: any, res: any) => {
    await employeeController.getAllEmployee(req,res);
});
router.get("/getAllWithProject",async (req: any, res: any) => {
    await employeeController.getAllEmployeeAssocatedWithProject(req,res);
});
router.get("/getEmpByName",async (req: any, res: any) => {
    await employeeController.getAllEmployeeByName(req,res);
});
router.get("/getEmpByPhone",async (req: any, res: any) => {
    await employeeController.getAllEmployeeByPhone(req,res);
});
router.post("/createproject",async (req: any, res: any) => {
    await projectController.createProject(req,res);
})
router.get("/getProjectByName",async (req: any, res: any) => {
    await projectController.getProjectByName(req,res);
});
router.put("/updateProjectStatus",async (req: any, res: any) => {
    await projectController.updateProjectStatus(req,res);
});

router.post("/assignProject",async (req: any, res: any) => {
    await projectAssignmentController.assignProjects(req,res);
})
router.delete("/deleteAssignProject",async (req: any, res: any) => {
    await projectAssignmentController.deleteAssignProject(req,res);
})

module.exports = router;