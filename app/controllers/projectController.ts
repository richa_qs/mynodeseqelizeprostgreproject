import {employees} from '../seeders/employees'
import {projects} from '../seeders/projects'
const {Projects,Employees, ProjectAssignment} = require( './../config/association')

exports.createProject =(req:any,res: any)=>{
  const project = 
    {
      title: req.body.title,
      status:  req.body.status
  }
    Projects.create(project).then((data: any) => {
      res.send(data);
    })
    .catch((err: { message: any; }) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the project."
      });
    });
  }

  exports.getProjectByName=(req:any,res: any)=>{
    Projects.findOne({ where: { title: req.query.title } }).then((data:any)=>{
      if (data === null) {
        console.log('Not found!');
      } else {
        console.log(data)
        res.send({
          message:"Updated status successfully of project id "+req.query.ProjectId 
        });
      }
  }).catch((err:any)=>{
    console.log(err)
  });
  }

  exports.updateProjectStatus=(req:any,res: any)=>{
    Projects.update({ status : req.body.status },{ where: { title: req.query.title } }).then((data:any)=>{
      if (data === null) {
        console.log('Not found!');
      } else {
        console.log(data)
        res.json(data)
      }
  }).catch((err:any)=>{
    console.log(err)
  });
  }