import {employees} from '../seeders/employees'
import {projects} from '../seeders/projects'
import {projectassignments} from '../seeders/projectassignments'
const {Projects,Employees, ProjectAssignment} = require( './../config/association')

exports.assignProjects =(req:any,res: any)=>{
    //console.log(db)
  const  projectassignment=  {
      ProjectId:req.body.ProjectId,
      EmployeeId:req.body.EmployeeId
  }
  
      ProjectAssignment.create(projectassignment).then((data: any) => {
        res.send(data);
      })
      .catch((err: { message: any; }) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while assigning project."
        });
      });
 
  }

  exports.deleteAssignProject=(req:any,res:any)=>{
    ProjectAssignment.destroy({
      where: {
        ProjectId: req.query.ProjectId
      }
    }).then((data: any) => {
      res.send({
        message:"Deteleted successfully with project id "+req.query.ProjectId 
      });
    })
    .catch((err: { message: any; }) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while assigning project."
      });
    });;
  }