import {employees} from '../seeders/employees'
const {Projects,Employees, ProjectAssignment} = require( './../config/association')

exports.createEmployee=(req:any,res: any)=>{
console.log(req.body)
  const employee = {
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      phone: req.body.phone
    };
    
        Employees.create(employee).then((data: any) => {
          res.send(data);
        })
        .catch((err: { message: any; }) => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while creating the employee."
          });
        });
   
}

exports.getAllEmployee=(req: any,res: { json: (arg0: any) => void })=>{
    Employees.findAll({
      }).then((result: any)=>{
       res.json(result)
      }).catch((err: any)=>{
        console.log(err)
      })
}

exports.getAllEmployeeByName=(req:any,res:any)=>
{
    Employees.findOne({ where: { firstname: req.query.firstname } }).then((data:any)=>{
      if (data === null) {
        console.log('Not found!');
      } else {
        console.log(data)
        res.json(data)
      }
  }).catch((err:any)=>{
    console.log(err)
  });
  
}
exports.getAllEmployeeByPhone=(req:any,res:any)=>
{
    Employees.findOne({ where: { phone: req.query.phone } }).then((data:any)=>{
      if (data === null) {
        console.log('Not found!');
      } else {
        console.log(data)
        res.json(data)
      }
  }).catch((err:any)=>{
    console.log(err)
  });
  
}

exports.getAllEmployeeAssocatedWithProject=(req: any,res: { json: (arg0: any) => void })=>{
    Employees.findAll({
        include:{
          model:Projects
        }
      }).then((result: any)=>{
       res.json(result)
      }).catch((err: any)=>{
        console.log(err)
      })
}